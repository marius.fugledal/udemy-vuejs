const app = Vue.createApp({
    data() {
        return {
            friends: [
                {
                    id: 'manuel',
                    name: 'Manuel Lorenz',
                    phone: '01234 5678 991',
                    email: 'manuel@localhost.com'
                },
                {
                    id: 'julie',
                    name: 'Julie Jones',
                    phone: '09876 543 221',
                    email: 'julie@localhost.com'
                },
            ],
        }
    }
});

app.component('user-contact', {
    template: `
        <li>
          <h2>{{ friend.name }}</h2>
          <button @click="toggleShowDetails">
            {{ detailsAreVisible ? 'Hide' : 'Show' }} Details
          </button>
          <ul v-if="detailsAreVisible">
            <li><strong>{{ friend.phone }}:</strong> 01234 5678 991</li>
            <li><strong>{{ friend.email }}:</strong> manuel@localhost.com</li>
          </ul>
        </li>
    `,
    data() {
        return {
            detailsAreVisible: true,
            friend: {
                id: 'manuel',
                name: 'Manuel Lorenz',
                phone: '01234 5678 991',
                email: 'manuel@localhost.com'
            },
        };
    },
    methods: {
        toggleShowDetails() {
            this.displayDetails = !this.displayDetails
        }
    }
});

app.mount('#app');