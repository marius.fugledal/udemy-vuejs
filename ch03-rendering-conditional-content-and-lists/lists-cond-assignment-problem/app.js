const app = Vue.createApp({
    data() {
        return {
            tasks: [],
            taskInput: '',
            showList: true
        }
    },
    computed: {
        buttonLabel() {
            return this.showList ? 'Hide List' : 'Show List'
        }
    },
    methods: {
        addTask() {
            this.tasks.push(this.taskInput)
        },
        toggleShowList() {
            this.showList = !this.showList
        }
    }
});

app.mount('#assignment');